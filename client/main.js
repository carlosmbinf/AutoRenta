var main = function(ct,code){
if (code === 1) {
    // Busca un traslado en coche privado, tren o autobús

    return  ` <main class="main d-flex align-items-center d-flex justify-content-center">
<div class="d-flex align-items-center d-flex justify-content-center">
    <div ct-app><noscript>YOUR BROWSER DOES NOT SUPPORT JAVASCRIPT</noscript></div>
</div>

<script>\n`+ ct +`\n            
    // Loads the GT Engine
    
      var cts = document.createElement('script');
      cts.type = 'text/javascript';
      cts.async = true;
      cts.src = '//ajaxgeo.cartrawler.com/loader-gt-5.0/ct_loader.js?' + new Date().getTime();

      var s = document.getElementsByTagName('script')[0];
      s.parentNode.insertBefore(cts, s);
    
  </script>

</main>`
}else if(code === 2){
// Buscar alquiler de coche

return `<main class="main2 d-flex align-items-center d-flex justify-content-center">
<div class="d-flex align-items-center d-flex justify-content-center">
  <div ct-app><noscript>YOUR BROWSER DOES NOT SUPPORT JAVASCRIPT</noscript></div>
</div>

<script>\n`+ ct +`\n 
  // Booking Engine Loader.
      CT.ABE.Settings.version = '5.0';
      var cts = document.createElement('script'); cts.type = 'text/javascript'; cts.async = true;
      cts.src = '//ajaxgeo.cartrawler.com/abe' + CT.ABE.Settings.version + '/ct_loader.js?' + new Date().getTime();
      var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(cts, s);
</script>

</main>`
    
}else{
    return ''
}
    
}

module.exports = main;