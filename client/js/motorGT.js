// GT engine settings object:            
var CT = {
    ABE: {
      Settings: {
        appType: 'gt',// Mandatory, always 'gt'
        clientID: process.env.clientID,// Mandatory, replace with your cartrawler's gt clientID/partnerID
        supportNo: '123123123',// replace with phone number for support
        rtl: false,// Mandatory, set to true when you want the page to read from Right to Left, i.e.: arabic
        language: 'ES',// start language, optional (obtained by IP if not specified)
        currency: 'EUR',// start currency, optional (obtained by IP if not specified)
        webkit: false,// Mandatory, always false, loads all the css,
        showAgePicker: false, //optional, displays a agepicker select instead of the passenger dropdownlist
        search: {
          deeplinkURL: ''// page address, specify if you want to redirect to a new page after the user hits 'search'
        },
        events: {
          ready: function (ev, status, data, api) {
            api.on('interstitial/show', function(ev, data) {
              alert('interestitial');
            })
          },
          results: function () {
              console.log('results!!!');
          },
          details: function () {
              console.log('details!!!');
          },
          payment: function () {
              console.log('payment!!!');
          },
          confirmation: function () {
              console.log('confirmation!!!');
          }
        }
      }
    }
  };
  
module.exports = CT