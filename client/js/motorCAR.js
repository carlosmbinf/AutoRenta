// Booking Engine Settings.
var CT = {
  ABE: {
      Settings: {
          clientID: process.env.clientID,
          language: 'ES',// start language, optional (obtained by IP if not specified)
          currency: process.env.currency,// start currency, optional (obtained by IP if not specified)
          webkit: false,// Mandatory, always false, loads all the css,
          showAgePicker: false, //optional, displays a agepicker select instead of the passenger dropdownlist
          search: {
          deeplinkURL: ''// page address, specify if you want to redirect to a new page after the user hits 'search'
        },
          events: {
              ready: function (event, status, data, api) {
                   // Place your code here.
              },
              searchcars: function(event, status, data, api) {
                   // Place your code here.
              },
              vehicles: function(event, status, data, api) {
                   // Place your code here.
              },
              vehicle: function(event, status, data, api) {
                   // Place your code here.
              },
              details: function (event, status, data, api) {
                   // Place your code here.
              },
              payment: function (event, status, data, api) {
                   // Place your code here.
              },
              confirmation: function(event, status, data, api) {
                   // Place your code here.
              }
          }
      }
  }
};

module.exports = CT