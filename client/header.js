var en1 = `

    <a href="">ABOUT US</a>
    <a href="">FREQUENTLY ASKED QUESTION</a>
    <a href="https://members.vacancyrewards.com/en/rewards">ELECTRONIC REWARDS</a>
    <a href="/?ln=en">TRANSFERS</a>
    <a href="/alquiler/?ln=en">CAR RENTAL</a>
    <a href="https://members.vacancyrewards.com/en/contact-us">CONTACT</a>
`
var es1 = `
    <a href="">QUIENES SOMOS</a>
    <a href="">PREGUNTA FRECUENTES</a>
    <a href="https://members.vacancyrewards.com/es/rewards">RECOMPENSAS ELECTRÓNICA</a>
    <a href="/?ln=es">TRASLADOS</a>
    <a href="/alquiler/?ln=en">RENTA DE AUTOS</a>
    <a href="https://members.vacancyrewards.com/es/contacto">CONTACTO</a>
`
var header = function(lang){
    
        if (lang === 'en') {
          return  `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes, minimal-ui">
    <title>Vacancy Rewards</title>
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css" integrity="sha384-QokYePQSOwpBDuhlHOsX0ymF6R/vLk/UQVz3WHa6wygxI5oGTmDTv8wahFOSspdm" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/fontawesome.css" integrity="sha384-vd1e11sR28tEK9YANUtpIOdjGW14pS87bUBuOIoBILVWLFnS+MCX9T6MMf0VdPGq" crossorigin="anonymous">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/brands.css" integrity="sha384-n9+6/aSqa9lBidZMRCQHTHKJscPq6NW4pCQBiMmHdUCvPN8ZOg2zJJTkC7WIezWv" crossorigin="anonymous">

    <!-- <script src="/js/motorGT.js"></script> -->
    <script src="/js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="/js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</head>
<body> 
    <header>
        <div class="container d-flex justify-content-between align-items-center" style="height: 100%;">
        <a href='https://members.vacancyrewards.com/en'><img src="/img/LogoVacancy.png" alt="Error cargando Imagen" ></a>

            <div>
                <div class="row contenedoresdebotonesblancos d-flex justify-content-center">
                    <div class="btn-group" style="border-right-style: groove;">
                        
                        <button type="button" class="btn dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Lenguaje
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                        <a class="dropdown-item" href="?ln=es">Español</a>
                        <a class="dropdown-item" href="?ln=en">English</a>
                        </div>
                      </div>
                      <div class="">
                        <a class="btn " href="https://members.vacancyrewards.com/es" >
                          Home
                        </a>
                      
                      </div>
                </div>
                <div class="d-flex align-items-center justify-content-center">
                    <div class="contacto d-flex align-items-center">
                    <div class="icon d-flex align-items-center justify-content-center" >
                        <i class="fa fa-phone" style="transform: rotate(102.23deg);"></i>
                    </div>
                    <div class="d-flex justify-content-center" style="position: relative;
                    right: -9px;">01 800 872 09 09 </div>
                    
                </div>
                </div>
                
            </div>
        </div>
    </header>
    <div class="header2 d-flex justify-content-center align-items-center">`
    +en1+
    `</div>`
        }else{

            return `<!DOCTYPE html>
            <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes, minimal-ui">
                <title>Document</title>
                <link rel="stylesheet" href="/css/app.css">
                <link rel="stylesheet" href="/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css" integrity="sha384-QokYePQSOwpBDuhlHOsX0ymF6R/vLk/UQVz3WHa6wygxI5oGTmDTv8wahFOSspdm" crossorigin="anonymous">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/fontawesome.css" integrity="sha384-vd1e11sR28tEK9YANUtpIOdjGW14pS87bUBuOIoBILVWLFnS+MCX9T6MMf0VdPGq" crossorigin="anonymous">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/brands.css" integrity="sha384-n9+6/aSqa9lBidZMRCQHTHKJscPq6NW4pCQBiMmHdUCvPN8ZOg2zJJTkC7WIezWv" crossorigin="anonymous">
            
                <!-- <script src="/js/motorGT.js"></script> -->
                <script src="/js/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
            <script src="/js/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
            <script src="/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
            </head>
            <body> 
                <header>
                    <div class="container d-flex justify-content-between align-items-center" style="height: 100%;">
                    <a href='https://members.vacancyrewards.com/es'><img src="/img/LogoVacancy.png" alt="Error cargando Imagen" ></a>
            
                        <div>
                            <div class="row contenedoresdebotonesblancos d-flex justify-content-center">
                                <div class="btn-group" style="border-right-style: groove;">
                                    
                                    <button type="button" class="btn dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Lenguaje
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu">
                                    <a class="dropdown-item" href="?ln=es">Español</a>
                                    <a class="dropdown-item" href="?ln=en">English</a>
                                    </div>
                                  </div>
                                  <div class="">
                                    <a class="btn " href="https://members.vacancyrewards.com/es" >
                                      Home
                                    </a>
                                  
                                  </div>
                            </div>
                            <div class="contacto row d-flex align-items-center">
                                <div class="icon d-flex align-items-center justify-content-center" >
                                    <i class="fa fa-phone" style="transform: rotate(102.23deg);"></i>
                                </div>
                                <div class="d-flex justify-content-center" style="position: relative;
                                right: -9px;">01 800 872 09 09 </div>
                                
                            </div>
                        </div>
                    </div>
                </header>
                <div class="header2 d-flex justify-content-center align-items-center">`
                +es1+
                `</div>`



        }
    

}

  module.exports = header;