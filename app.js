var express = require('express');
var cookieParser = require('cookie-parser')
var app = express();
app.use(cookieParser());
require('dotenv').config({path:'./variables.env'})

// Estructura del html
var header = require('./client/header.js');
var main = require('./client/main.js');
var footer = require('./client/footer.js');

const hostname = '127.0.0.1';
const port = process.env.PORT || 3000;

var fs = require('fs');
app.use('/css', express.static(__dirname + '/client/css'));
app.use('/img', express.static(__dirname + '/client/img'));
app.use('/js', express.static(__dirname + '/client/js'));
// app.use('/font', express.static(__dirname + '/client/font'));

app.get('/', function (req, res) {
  var CT = require('./client/js/motorGT.js')

  if (req.query.ln == 'en') {    
    CT.ABE.Settings.language = 'EN'

    var ct = JSON.stringify(CT).toString();
  
  res.write(header('en',1));
  res.write(main('var CT = ' + ct,1 ));
  res.write(footer('en',1));
  // res.sendFile(__dirname + '/client/index.html');
  res.end();

  }else{    
    CT.ABE.Settings.language = 'ES'

    var ct = JSON.stringify(CT).toString();
    
    
  res.write(header('es',1));
  res.write(main('var CT = ' + ct,1 ));
  res.write(footer('es',1));
  // res.sendFile(__dirname + '/client/index.html');
  res.end();

  }
  
  
});

app.get('/alquiler', function (req, res) {
  // res.sendFile(__dirname + '/client/index2.html');
  var CT = require('./client/js/motorCAR.js')
  var ct = ''
  if (req.query.ln == 'en') {
    CT.ABE.Settings.language = 'EN'
    ct = JSON.stringify(CT).toString();
    res.write(header('en',2));
    res.write(main('var CT = ' + ct,2 ));
    res.write(footer('en',2));
    res.end();

  }else{
    CT.ABE.Settings.language = 'ES'
    ct = JSON.stringify(CT).toString();
    res.write(header('es'));
    res.write(main('var CT = ' + ct,2 ));
    res.write(footer('es'));
    res.end();
  
  }

});

//   if(req.url === '/'){
//   fs.createReadStream(__dirname + '/client/index.html').pipe(res);
//   }

//  else if(req.url === '/version'){

//     res.writeHead(200,{'Content-Type': 'application/json'});
//     var obj = {
//       firstname: 'Carlos',
//       lastname: 'Medina'
//     }
//     res.end(JSON.stringify(obj));
//   }else{
//     res.writeHead(404)
//     res.end();
//   }

 

// server.listen(port, hostname, () => {
//   console.log(`Server running at http://${hostname}:${port}/`);
// });

app.listen(port, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
  })